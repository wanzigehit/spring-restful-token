package com.molyfun.web.base;

import com.molyfun.model.student.MhStudent;
import com.molyfun.model.user.HoUser;

/**

 * 创建日期：2016-7-8下午3:07:23

 * 作者：石冬生

 */

public class UserThreadLocal {

    private static final ThreadLocal<HoUser> LOCAL = new ThreadLocal<HoUser>();

    public static void set(HoUser user) {
        LOCAL.set(user);
    }

    public static HoUser get() {
        return LOCAL.get();
    }

}