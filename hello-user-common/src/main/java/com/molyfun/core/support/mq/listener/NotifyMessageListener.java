package com.molyfun.core.support.mq.listener;

import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import javax.jms.MapMessage;
import javax.jms.Message;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import javax.jms.MessageListener;


/**
 * 消息的异步被动接收者.
 * 
 * 使用Spring的MessageListenerContainer侦听消息并调用本Listener进行处理.
 * 
 * @author calvin
 */
public class NotifyMessageListener implements MessageListener {

	private static Logger logger = LoggerFactory.getLogger(NotifyMessageListener.class);

//	@Autowired(required = false)
//	private SimpleMailService simpleMailService;
	/**
	 * MessageListener回调函数.
	 */
	@Override
	public void onMessage(Message message) {
		try {
			MapMessage mapMessage = (MapMessage) message;
			// 打印消息详情
//			logger.info("UserName:{}, Email:{}", mapMessage.getString("userName"), mapMessage.getString("email"));
			System.out.println("UserName:{}, Email:{}  message listener:"+ mapMessage.getString("phone")+ mapMessage.getString("content"));
			// 发送邮件
//			SmsUtil.sendSms(mapMessage.getString("phone"),mapMessage.getString("content"));
		} catch (Exception e) {
			logger.error("处理消息时发生异常.", e);
		}
	}
}
