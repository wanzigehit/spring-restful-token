package com.molyfun.core.support.mq.listener;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.molyfun.core.support.email.Email;
import com.molyfun.core.support.mq.vo.Sms;
import com.molyfun.core.util.EmailUtil;
import com.molyfun.core.util.SmsUtil;

/**
 * 发送邮件队列
 * 
 */
public class SmsListener implements MessageListener {
	private final Logger logger = LogManager.getLogger();

	public void onMessage(Message message) {
		try {
			Sms sms = (Sms) ((ObjectMessage) message).getObject();
			SmsUtil.sendSms(sms);
		} catch (JMSException e) {
			logger.error(e);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
