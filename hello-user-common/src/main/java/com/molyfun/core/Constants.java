package com.molyfun.core;

/**
 * 常量表
 */
public interface Constants {
	/**
	 * 异常信息统一头信息<br>
	 * 非常遗憾的通知您,程序发生了异常
	 */
	public static final String Exception_Head = "OH,MY GOD! SOME ERRORS OCCURED! AS FOLLOWS :";
	/** 客户端语言 */
	public static final String USERLANGUAGE = "userLanguage";
	/** 客户端主题 */
	public static final String WEBTHEME = "webTheme";
	/** 当前用户 */
	public static final String CURRENT_USER = "CURRENT_USER";
	/** 在线用户数量 */
	public static final String ALLUSER_NUMBER = "ALLUSER_NUMBER";
	/** 登录用户数量 */
	public static final String USER_NUMBER = "USER_NUMBER";
	/** 上次请求地址 */
	public static final String PREREQUEST = "PREREQUEST";
	/** 上次请求时间 */
	public static final String PREREQUEST_TIME = "PREREQUEST_TIME";
	/** 非法请求次数 */
	public static final String MALICIOUS_REQUEST_TIMES = "MALICIOUS_REQUEST_TIMES";
	/** 缓存命名空间 */
	public static final String CACHE_NAMESPACE = "molyfun:";
	/** 缓存命名空间 */
	public static final String CACHE_TOKEN = "token:";
	public static final String CAPTCHA_CODE = "captcha:";
	//短信验证码获取最小间隔时间
	public static final long CAPTCHA_INTERVAL_TIME = 60*1000;
	//短信验证码获取最小间隔时间
	public static final long CAPTCHA_EXPIRE_TIME = 600*1000;
	/** 密码加密字符串 */
	public static String ValidStr="molyfun";
}
